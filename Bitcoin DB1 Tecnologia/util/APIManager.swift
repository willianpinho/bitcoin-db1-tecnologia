//
//  APIManager.swift
//  Bitcoin DB1 Tecnologia
//
//  Created by Willian Pinho on 31/01/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class APIManager {
    
    static let shared = APIManager()
    
    private let manager: Alamofire.SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = APIConstants.kTimeOutPeriod
        configuration.timeoutIntervalForResource = APIConstants.kTimeOutPeriod
        return Alamofire.SessionManager(configuration: configuration)
    }()
    
    static func getRequest(forUrl url: String, withParameters parameters: [String : Any]? = nil, inBody:Bool=false, haveHeader: Bool? = nil, completion: @escaping (_ success: Bool, _ message: String?, _ response: Dictionary<String, AnyObject>?) -> Void) {
        
        let headers = generateHeaderRequest(haveHeader, parameters)
        
        var enconding:ParameterEncoding = URLEncoding.default
        
        if inBody {
            enconding = JSONEncoding.default
        }
        
        self.request(url, forMethod: .get, withParameters: parameters, withEncoding: enconding, withHeaders: headers) { (success, message, response) in
            completion(success,message,response)
        }
    }
    
    private static func generateHeaderRequest(_ haveHeader: Bool? = nil, _ parameters: [String:Any]? = nil) -> [String: String]?{
        
        let currentParameters = parameters
        
        return currentParameters as? [String : String]
    }
    
    
    private static func request(_ url: String, forMethod method: HTTPMethod, withParameters parameters: [String : Any]? = nil, withEncoding encoding:ParameterEncoding=URLEncoding.default, withHeaders headers: [String : String]? = nil, completion: @escaping (_ success: Bool, _ message: String?, _ response: Dictionary<String, AnyObject>?) -> Void) {
        
        Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response: DataResponse<Any>) in
            switch  response.result {
                
            case .success:
                
                let data = response.result.value as! Dictionary<String, AnyObject>
                
                switch response.response!.statusCode {
                    
                case 200:
                    completion(true,nil,data)
                case 500:
                    completion(false, APIConstants.kMessageIncorrectNumber, nil)
                default:
                    
                    completion(false, APIConstants.kMessageServerUnavailable,nil)
                }
                
            case .failure(_):
                
                completion(false,APIConstants.kMessageNetworkError,nil)
            }
        }
    }
}
