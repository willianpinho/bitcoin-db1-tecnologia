//
//  APIConstants.swift
//  Bitcoin DB1 Tecnologia
//
//  Created by Willian Pinho on 31/01/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation

struct APIConstants {
    static let kTimeOutPeriod:TimeInterval = 10
    static let kKeyTokenHeader = "token"
    static let kKeyStatus = "status"
    static let kKeyMessage = "message"
    static let kKeyData = "data"
    static let kMessageNetworkError = "Sem conexão com a Internet."
    static let kMessageServerUnavailable = "Servidor Indisponível. Tente novamente mais tarde."
    static let kMessageIncorrectNumber = "Número de Telefone Incorreto"
    static let kMessageError = "Erro"
}
