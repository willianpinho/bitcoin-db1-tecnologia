//
//  Endpoints.swift
//  Bitcoin DB1 Tecnologia
//
//  Created by Willian Pinho on 31/01/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation

struct API {
    static let baseUrl = "https://api.blockchain.info"
    
    static func createUrl(path: String) -> String {
        return "\(API.baseUrl)\(path)"
    }
}

protocol Endpoint {
    var path: String { get }
    var url: String { get }
    
}

enum Endpoints {
    
    enum Charts: Endpoint {
        case marketPrice
        
        public var path: String {
            switch self {
            case .marketPrice: return "/charts/market-price"
            }
        }
        
        public var url: String {
            switch self {
            case .marketPrice: return API.createUrl(path: Endpoints.Charts.marketPrice.path)
            }
        }
    }
    
    enum Stats: Endpoint {
        case root
        
        public var path: String {
            switch self {
            case .root: return "/stats"
            }
        }
        
        public var url: String {
            switch self {
            case .root: return API.createUrl(path: Endpoints.Stats.root.path)
            }
        }
    }
}
