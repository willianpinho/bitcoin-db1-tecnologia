//
//  Chart.swift
//  Bitcoin DB1 Tecnologia
//
//  Created by Willian Pinho on 31/01/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import ObjectMapper
import ObjectMapperAdditions
import RealmSwift
import RealmAdditions

class Chart: Object, Mappable {
    var status: String?
    var name: String?
    var unit: String?
    var period: String?
    var descriptionChart: String?
    var values: [Value]?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        name <- map["name"]
        unit <- map["unit"]
        period <- map["period"]
        descriptionChart <- map["description"]
        values <- map["values"]
    }
}
