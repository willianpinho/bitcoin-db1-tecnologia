//
//  BitcoinStat.swift
//  Bitcoin DB1 Tecnologia
//
//  Created by Willian Pinho on 31/01/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import ObjectMapper
import ObjectMapperAdditions
import RealmSwift
import RealmAdditions


class BitcoinStat : Object, Mappable {
    var timestamp: Double?
    var marketPriceUSD: Double?
    var hashRate: Double?
    var totalFeesBTC: Double?
    var BTCMined: Double?
    var tx: Double?
    var blocksMined: Double?
    var minutesBetweenBlocks: Double?
    var totalBC: Double?
    var totalBlocks: Double?
    var estimatedTransactionVolumeUSD: Double?
    var blocksSize: Double?
    var minersRevenueUSD: Double?
    var nextTRETarget: Double?
    var difficulty: Double?
    var estimatedBTCSent: Double?
    var minersRevenueBTC: Double?
    var totalBTCSent: Double?
    var tradeVolumeBTC: Double?
    var tradeVolumeUSD: Double?
    
    required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        timestamp <- map["timestamp"]
        marketPriceUSD <- map["market_price_usd"]
        hashRate <- map["hash_rate"]
        totalFeesBTC <- map["total_fees_btc"]
        BTCMined <- map["n_btc_mined"]
        tx <- map["n_tx"]
        blocksMined <- map["n_blocks_mined"]
        minutesBetweenBlocks <- map["minutes_between_blocks"]
        totalBC <- map["totalbc"]
        totalBlocks <- map["n_blocks_total"]
        estimatedTransactionVolumeUSD <- map["estimated_transaction_volume_usd"]
        blocksSize <- map["blocks_size"]
        minersRevenueUSD <- map["miners_revenue_usd"]
        nextTRETarget <- map["nextretarget"]
        difficulty <- map["difficulty"]
        estimatedBTCSent <- map["estimated_btc_sent"]
        minersRevenueUSD <- map["miners_revenue_btc"]
        totalBTCSent <- map["total_btc_sent"]
        tradeVolumeBTC <- map["trade_volume_btc"]
        tradeVolumeUSD <- map["trade_volume_usd"]
    }
    
}
