//
//  BitcoinStatService.swift
//  Bitcoin DB1 Tecnologia
//
//  Created by Willian Pinho on 31/01/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import ObjectMapper
import AlamofireObjectMapper

class BitcoinStatService {
    static func getBitcoinStats(completion: @escaping (_ success: Bool, _ message: String?, _ response: BitcoinStat?) -> Void) {
        
        APIManager.getRequest(forUrl: Endpoints.Stats.root.url) { (success, message, response) in
            if success {
                let json = JSON(response!)
                let stat = Mapper<BitcoinStat>().map(JSONString: json.rawString()!)
                completion(true, message, stat)
            } else {
                completion(false, message, nil)
            }
        }
    }
}
