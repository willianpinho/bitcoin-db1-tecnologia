//
//  ChartService.swift
//  Bitcoin DB1 Tecnologia
//
//  Created by Willian Pinho on 31/01/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import ObjectMapper
import AlamofireObjectMapper

class ChartService {
    static func getBitcoinMarketPriceUSD(completion: @escaping (_ success: Bool, _ message: String?, _ response: Chart?) -> Void) {
        var parameters = Parameters()
        parameters["timespan"] = "15days"
        
        APIManager.getRequest(forUrl: Endpoints.Charts.marketPrice.url, withParameters: parameters) { (success, message, response) in
            if success {
                let json = JSON(response!)
                let chart = Mapper<Chart>().map(JSONString: json.rawString()!)
                completion(true, message, chart)
            } else {
                completion(false, message, nil)
            }
        }
    }
}

