//
//  BitcoinPresenter.swift
//  Bitcoin DB1 Tecnologia
//
//  Created by Willian Pinho on 31/01/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation

protocol BitcoinDetailsView: NSObjectProtocol {
    func showLoading()
    func hideLoading()
    func reloadTableView()
    func showAlert(title: String, message: String)
    func setupBitcoinStat(stats: BitcoinStat)
}

class BitcoinDetailsPresenter: NSObject {
    var view: BitcoinDetailsView?

    func setViewDelegate(view: BitcoinDetailsView) {
        self.view = view
    }
    
    func loadBitcoinStats() {
        BitcoinStatService.getBitcoinStats { (success, message, stats) in
            if success {
                self.view?.showLoading()
                self.view?.setupBitcoinStat(stats: stats!)
                self.view?.reloadTableView()
            } else {
                self.view?.showLoading()
                self.view?.showAlert(title: "Error", message: message!)
            }
            self.view?.hideLoading()
        }
    }
    
}
