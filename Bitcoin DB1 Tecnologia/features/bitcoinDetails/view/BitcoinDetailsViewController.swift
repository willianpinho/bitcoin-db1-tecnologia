//
//  BitcoinDetailsViewController.swift
//  Bitcoin DB1 Tecnologia
//
//  Created by Willian Pinho on 31/01/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit
import PKHUD

class BitcoinDetailsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var presenter = BitcoinDetailsPresenter()
    var cells: [UITableViewCell] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setPresenterDelegate()
        self.setupTableView(tableView: self.tableView)
        self.customLayout()
        self.presenter.loadBitcoinStats()
    }
    
    func setPresenterDelegate() {
        presenter.setViewDelegate(view: self)
    }
    
    func customLayout() {
        self.removeNavBarLine()
        self.setNavigation(title: "Bitcoin Stats", withTintColor: .black, barTintColor: .white, andAttributes: [NSAttributedStringKey.font: UIFont.openSansLight(withSize: 24), NSAttributedStringKey.foregroundColor: UIColor.black], prefersLargeTitles: false)
    }

}

extension BitcoinDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func setupTableView(tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerNibs([
            BitcoinDetailsTableViewCell.self
            ])
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cells[indexPath.row]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
}

extension BitcoinDetailsViewController: BitcoinDetailsView {
    func addCellsToTableView() {
        
    }
    
    func setupBitcoinStat(stats: BitcoinStat) {
        let mirror = Mirror(reflecting: stats)
        
        for case let (name, value) in mirror.children {
            guard let name = name else {
                continue
            }
            if let bdtvc = self.tableView.dequeueReusableCell(withIdentifier: "BitcoinDetailsTableViewCell") as? BitcoinDetailsTableViewCell {
                bdtvc.titleLabel.text = name
                bdtvc.valueLabel.text = (value as AnyObject).stringValue
                cells.append(bdtvc)
            }
        }
    }
    
    func showLoading() {
        PKHUD.sharedHUD.show()
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide()
    }
    
    func reloadTableView() {
        self.tableView.reloadData()
    }
    
    func showAlert(title: String, message: String) {
        let alert = Alert.showMessage(title: title, message: message)
        self.present(alert, animated: true, completion: nil)
    }
    
}
