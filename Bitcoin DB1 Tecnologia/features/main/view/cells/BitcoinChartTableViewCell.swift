//
//  BitcoinChartTableViewCell.swift
//  Bitcoin DB1 Tecnologia
//
//  Created by Willian Pinho on 31/01/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit
import Charts

class BitcoinChartTableViewCell: UITableViewCell {

    @IBOutlet weak var chart: BarChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
