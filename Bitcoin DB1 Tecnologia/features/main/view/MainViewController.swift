//
//  ViewController.swift
//  Bitcoin DB1 Tecnologia
//
//  Created by Willian Pinho on 29/01/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit
import PKHUD
import Charts

class MainViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var presenter = MainPresenter()
    var cells: [UITableViewCell] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setPresenterDelegate()
        self.setupTableView(tableView: self.tableView)
        self.customLayout()
        self.presenter.loadBitcoinStats()
        self.presenter.loadMarketPrice()
    }
    
    func setPresenterDelegate() {
        presenter.setViewDelegate(view: self)
    }
    
    func customLayout() {
        self.removeNavBarLine()
        self.setNavigation(title: "Current Bitcoin Value", withTintColor: .black, barTintColor: .white, andAttributes: [NSAttributedStringKey.font: UIFont.openSansLight(withSize: 24), NSAttributedStringKey.foregroundColor: UIColor.black], prefersLargeTitles: false)
    }
    
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func setupTableView(tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerNibs([
            BitcoinCardTableViewCell.self,
            BitcoinChartTableViewCell.self
            ])
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cells[indexPath.row]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = cells[indexPath.row]
        if cell.isKind(of: BitcoinCardTableViewCell.self) {
            self.openRootViewController(storyBoard: "BitcoinDetails")
        }
    }
}

extension MainViewController: MainView {
    func showLoading() {
        PKHUD.sharedHUD.show()
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide()
    }
    
    func reloadTableView() {
        self.tableView.reloadData()
    }
    
    func showAlert(title: String, message: String) {
        let alert = Alert.showMessage(title: title, message: message)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func setupBitcoinCard(stats: BitcoinStat) {
        if let btcctvc = self.tableView.dequeueReusableCell(withIdentifier: "BitcoinCardTableViewCell") as? BitcoinCardTableViewCell {
            if let currentValue = stats.marketPriceUSD {
                btcctvc.currentValue.text = "US$" + String(format: "%.2f", currentValue)
            }
            cells.append(btcctvc)
        }
    }
    
    func setupChart(chart: Chart) {
        if let btcctvc = self.tableView.dequeueReusableCell(withIdentifier: "BitcoinChartTableViewCell") as? BitcoinChartTableViewCell {
            btcctvc.chart = self.setupChatView(chartValue: chart, barChartView: btcctvc.chart)
            cells.append(btcctvc)
        }
    }
    
    func setupChatView(chartValue: Chart, barChartView: BarChartView) -> BarChartView? {
        barChartView.noDataText = "No Data to show :("
        if let descriptionText = chartValue.descriptionChart {
            barChartView.chartDescription?.text = descriptionText
        }

        
        if let values = chartValue.values {
            let xAxis = barChartView.xAxis
            xAxis.labelPosition = .bottom
            xAxis.labelFont = .systemFont(ofSize: 10)
            xAxis.granularity = 1
            xAxis.labelCount = 7
            
            let leftAxisFormatter = NumberFormatter()
            leftAxisFormatter.minimumFractionDigits = 0
            leftAxisFormatter.maximumFractionDigits = 1
            leftAxisFormatter.negativeSuffix = " $"
            leftAxisFormatter.positiveSuffix = " $"
            
            
            let leftAxis = barChartView.leftAxis
            leftAxis.labelFont = .systemFont(ofSize: 10)
            leftAxis.labelCount = 8
            leftAxis.valueFormatter = DefaultAxisValueFormatter(formatter: leftAxisFormatter)
            leftAxis.labelPosition = .outsideChart
            
            
            var dataEntries: [BarChartDataEntry] = []

            for i in 0..<values.count {
                if let y = values[i].y {
                    let dataEntry = BarChartDataEntry(x: Double(i), y: y)
                    dataEntries.append(dataEntry)
                }
            }
            
            let chartDataSet = BarChartDataSet(values: dataEntries, label: "Bitcoin in last 15 days")
            chartDataSet.colors = ChartColorTemplates.colorful()
            let chartData = BarChartData(dataSets: [chartDataSet])
            chartData.barWidth = 1
            barChartView.data = chartData
            
            barChartView.drawBarShadowEnabled = false
            barChartView.drawValueAboveBarEnabled = false
        }
        return barChartView
    }
    
    
}

