//
//  MainPresenter.swift
//  Bitcoin DB1 Tecnologia
//
//  Created by Willian Pinho on 31/01/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import UIKit

protocol MainView: NSObjectProtocol {
    func showLoading()
    func hideLoading()
    func reloadTableView()
    func showAlert(title: String, message: String)
    func setupChart(chart: Chart)
    func setupBitcoinCard(stats: BitcoinStat)
}

class MainPresenter: NSObject {
    var view: MainView?
    
    func setViewDelegate(view: MainView) {
        self.view = view
    }
    
    func loadMarketPrice() {
        ChartService.getBitcoinMarketPriceUSD { (success, message, chart) in
            if success {
                self.view?.showLoading()
                self.view?.setupChart(chart: chart!)
                self.view?.reloadTableView()
            } else {
                self.view?.showLoading()
                self.view?.showAlert(title: "Error", message: message!)
            }
            self.view?.hideLoading()
        }
    }
    
    func loadBitcoinStats() {
        BitcoinStatService.getBitcoinStats { (success, message, stats) in
            if success {
                self.view?.showLoading()
                self.view?.setupBitcoinCard(stats: stats!)
                self.view?.reloadTableView()
            } else {
                self.view?.showLoading()
                self.view?.showAlert(title: "Error", message: message!)
            }
            self.view?.hideLoading()
        }
    }
}
