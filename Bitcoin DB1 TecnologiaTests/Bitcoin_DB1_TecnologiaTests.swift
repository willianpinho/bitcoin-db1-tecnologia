//
//  Bitcoin_DB1_TecnologiaTests.swift
//  Bitcoin DB1 TecnologiaTests
//
//  Created by Willian Pinho on 29/01/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import XCTest
@testable import Bitcoin_DB1_Tecnologia

class Bitcoin_DB1_TecnologiaTests: XCTestCase {
    var sessionUnderTest: URLSession!

    override func setUp() {
        super.setUp()
        sessionUnderTest = URLSession(configuration: URLSessionConfiguration.default)
    }
    
    override func tearDown() {
        sessionUnderTest = nil
        super.tearDown()
    }
    
    // Testing Input Routes
    func testValidCallToStatsGetsHTTPStatusCode200() {
        let url = URL(string: Endpoints.Stats.root.url)
        let promise = expectation(description: "Status code: 200")
        let dataTask = sessionUnderTest.dataTask(with: url!) { data, response, error in
            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                return
            } else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                if statusCode == 200 {
                    promise.fulfill()
                } else {
                    XCTFail("Status code: \(statusCode)")
                }
            }
        }
        dataTask.resume()
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testValidCallToChartsGetsHTTPStatusCode200() {
        let url = URL(string: Endpoints.Charts.marketPrice.url)
        let promise = expectation(description: "Status code: 200")
        let dataTask = sessionUnderTest.dataTask(with: url!) { data, response, error in
            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                return
            } else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                if statusCode == 200 {
                    promise.fulfill()
                } else {
                    XCTFail("Status code: \(statusCode)")
                }
            }
        }
        dataTask.resume()
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
