# DB1 Tecnolgia - Projeto Mobile - Bitcoin


## Especificações

Crie uma aplicação mobile que consulte informações sobre a cotação do Bitcoin e mostre essas informações de uma maneira apropriada para um dispositivo mobile.

### A aplicação deve ter:
- Fazer uma chamada em uma API remota para buscar as informações de cotação (não é necessário demonstrar o processo de autenticação, este pode ser fixo).
- Salvar as informações localmente.
- Na mesma tela, mostrar a última cotaçnao em um card de destaque e a evolução da cotação de forma apropriada.

Desenvolva sua aplicação de uma forma que sejam demonstrados os conhecimentos sobre:

- Código limpo e testável;

- Padrões de Arquitetura;

- SOLID Design Principles

Importe:
- Coloque o seu projeto no Github, e tenha um histórico de commits que faça sentido.
- Você pode usar quaiquer libraries ou frameworks que quiser, mas lembre-se que é o seu código que está sendo avaliado.

Fontes sugeridas:

Blockchain Market Price Graph

[https://blockchain.info/charts/market-price]()

- Blockchain Charts API Documentation

	[https://blockchain.info/api/charts_api]()


## Instalação

### CocoaPods
Foi utilizado o [CocoaPods](https://guides.cocoapods.org/using/getting-started.html) para gerenciamento de dependências, você pode instalar seguindo as informações no site.
### Rodando a Aplicação
- Basta ir no terminal e digitar:

```
cd /caminho/do/seu/projeto
pod install
Abrir xcode
Be happy
```
### Arquitetura
Foi utilizado MVP para o desenvolvimento do projeto, você pode conhecer mais a arquitetura nessas fontes.

- [https://medium.com/ios-os-x-development/ios-architecture-patterns-ecba4c38de52]() 
- [https://apiumhub.com/tech-blog-barcelona/mvp-pattern-ios/]()

### Estrutura de Arquivos

```
.
├── Bitcoin DB1 Tecnologia
│   ├── util
│   │   ├── fonts
│   │   └── extensions
│   ├── service
│   ├── model
│   │   ├── fonts
│   │   └── extensions
│   ├── features
│   │   ├── main
│   │   │   ├─ view
│   │   │   │	  └── cells
│   │   │   └── presenter
│   │   └── bitcoinDetails
│   │   │   ├─ view
│   │   │   │	  └── cells
└───└───└───└─ presenter
```
#### Util 
Classes e extensões para ajudar no desenvolvimento.
#### Service 
Serviços para comunicação com a API.
#### Model 
Modelo de dados da Aplicação
#### Features 
Funções da aplicação temos a Main e detalhes do Bitcoin

